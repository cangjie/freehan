import logging
import sys
import yaml
import jsonschema
import argparse
from referencing import Registry, Resource
import os

# Parse command options
parser = argparse.ArgumentParser(description='Validate a YAML file against a JSON schema.')
parser.add_argument('--source-dir', '-d', default='./schemas', help='the source directory of the JSON schemas. Default: ./schemas')
parser.add_argument('--schema', '-s', choices=['characters'], default='characters', help='the JSON schema to use for validation. Default: characters')
parser.add_argument('file', type=argparse.FileType('r'), help='the YAML file to validate')
args = parser.parse_args()

# Create a logger that format logs with date and time information
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
logger = logging.getLogger()
logHandler = logging.StreamHandler(sys.stderr)
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)
logger.setLevel(logging.DEBUG)

# Create a registry and add schemas
# Note: validatation pass even if the schemas are not loaded or added to the registry
registry = Registry()

# Find all schema files (*.yml) in the source directory (args.source_dir)
schema_files = [filename for filename in os.listdir(args.source_dir) if filename.endswith('.yml')]
schemas = {}

# Load the schemas and add them to the registry
for schema_file in schema_files:
    # get the file name without extension
    schema_name = os.path.splitext(schema_file)[0]
    schema_path = os.path.join(args.source_dir, schema_file)
    with open(schema_path, 'r') as f:
        schema_data = yaml.safe_load(f)
        if '$id' not in schema_data:
            raise Exception("schema file %s does not have '$id' field" % schema_path)
        schemas[schema_name] = {
            "url": schema_data['$id'],
            "file": schema_path,
            "data": schema_data,
        }
        registry = registry.with_resource(schema_path, Resource.from_contents(schema_data))
        logger.debug("registry: load schema %s", schema_path)

logger.debug("registry: all schemas loaded")

# Validate the data
try:
    logger.debug("main: creating validator for schema %s", args.schema)
    validator = jsonschema.Draft202012Validator(schema=schemas[args.schema]['data'], registry=registry)

    # Validate the schemas
    for name in schemas:
        logger.debug("validator: validating schema %s", schemas[name]['file'])
        validator.check_schema(schemas[name]["data"])
        logger.debug("validator: validation succeeded")

    # Load the data to be validated
    logger.debug("main: opening %s", args.file.name)
    with open(args.file.name, 'r') as f:
        logger.debug("main: parsing %s", args.file.name)
        data = yaml.unsafe_load(f)
        logger.debug("main: opened and parsed %s", args.file.name)

    logger.debug("validator: validating data file")
    validator.validate(instance=data)
    logger.info("validator: data file validation succeeded")
except jsonschema.exceptions.ValidationError as err:
    logger.error("validation failed")
    logger.error(err)
    sys.exit(1)