# 自由漢語資料<br>Free Han Chinese Data

一套自由、開放、公共領域的漢語相關資料。內容包括萬國碼 (Unicode) 內含的漢字，與及詞語資料匯集。

A project to maintain a public domain repository of Han Chinese related data. Includes
data about Chinese characters in Unicode and vocabulary collection.

## 緣由<br>Why

華文地區所有關於中文文字、詞語的資料均基於受限的版權或授權宣告，這狀況阻礙了華文在電腦運算、分析的發展。Cangjians 團隊希望藉由開放授權本資料匯整，鼓勵開放的中文語素、文字資料交流與共享，減少重覆的中文資料、語素整理工作，以推動發展華文相關的電腦語文處理。

As you may aware of, all the currently available Han Chinese information are based on limited license agreements. This obscures the research and development of computation and analysis of Han Chinese characters and morpheme. By publishing a public domain data set, we hope to reduce the duplicated work of collecting and editing information around Chinese characters and vocabularies. And on this basis, we hope to encourage the exchange and sharing of these information and further the research and development of access and computation of Han Chinese.

## 維護及貢獻<br>Maintain and Contribute

本資料由[Cangjians 團隊][cangjians-team]在 [Freedesktop.org][freehan-link] 維護，歡迎任何人貢獻或建議修改。如有任何修正、問題或建議，請使用我們在 Gitlab 的[問題追蹤介面][issue-tracker]建立或加入相關討論。

This repository is maintained by [the Cangjians][cangjians-team] on [Freedesktop.org][freehan-link]. All is welcome to contribute or suggest changes. If you have any fix, question or suggestion, please create or join discussion on our Gitlab's [issue tracker][issue-tracker].

[cangjians-team]: https://cangjians.github.io
[freehan-link]: https://gitlab.freedesktop.org/cangjie/freehan
[issue-tracker]: https://gitlab.freedesktop.org/cangjie/freehan/issues

## 授權<br>License

本資料匯輯以 CC0 公共領域授權發佈，詳細授權協議請參詳 [LICENSE.md](LICENSE.md) 檔案內文，或參閱 [Creative Commons 網站][cc0-link]的相關介紹。

This data repository is published under the CC0 Public Domain license. You may read the details of the license in our [LICENSE.md](LICENSE.md). You may also read the [Creative Commons official site][cc0-link] to get more details.


[cc0-link]: https://creativecommons.org/share-your-work/public-domain/cc0/
