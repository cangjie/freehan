# Schemas for the Free Han Chinese Data project

This folder contains schema files for the Free Han Chinese Data project for
the development and maintenance of data format.

Schema files follows the [JSON Schema](https://json-schema.org/) Draft 202012
specification but stored in YAML format for the ease of read and comment.